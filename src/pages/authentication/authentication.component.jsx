import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect'
import { selectCurrentUser } from '../../redux/user/user.selectors'
import { setUser } from '../../redux/user/user.actions';
import FormInput from '../../components/form-input/form-input.component';
import Button from '../../components/button/button.component';

import './authentication.styles.scss';

class Authentication extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       email: '',
       password: ''
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const {path} = this.props.match
    const url = path === '/auth' ? "http://kiostani-api.herokuapp.com/users/login" : "http://kiostani-api.herokuapp.com/merchants/login";
    
    fetch(url, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body:  JSON.stringify(this.state)
    })
    .then(response => response.json())
    .then(data => {
      this.props.setUser(data);
    } )
    .catch(error => console.dir(error))

    this.setState({
      email: '',
      password: ''
    })
  }

  handleChange = event => {
    const {name, value} = event.target;
    this.setState({[name] : value })
  }

  render() {
    return (
      <div className="auth">
        <h3 className="auth__title"> Masuk </h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <FormInput required type="email" name="email" placeholder="other@example.com" label="Masukkan email" handleChange={this.handleChange} />
            <FormInput required type="password" name="password" placeholder="Masukkan Password" label="Masukkan Password" handleChange={this.handleChange} />
          </div>

          <div className="btn-group">
          <Button type="submit"> Masuk </Button>
          <p> Atau </p>
          <Button type="button" isSecondary> Daftar </Button>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser
})

const mapDispatchToProps = dispatch => ({
  setUser: user => dispatch(setUser(user))
})
export default connect(mapStateToProps, mapDispatchToProps)(Authentication);
