import React, {Component} from 'react';
import Categories from '../../components/categories/categories.component';
import Kentang from '../../assets/img/kentang.jpg';
import Jagung from '../../assets/img/jagung.jpeg';
import Padi from '../../assets/img/padi.jpg';
import Tomat from '../../assets/img/tomat.jpg';

import './dashboard.styles.scss';

class Dashboard extends Component{
  constructor(props) {
    super(props)
  
    this.state = {
      categories: [
        {
          id: 1,
          name: "Kentang",
          imgUrl: Kentang,
          link: "http://kiostani-api.herokuapp.com/products/q=Kentang"
        },
        {
          id: 2,
          name: "Tomat",
          imgUrl: Tomat,
          link: "http://kiostani-api.herokuapp.com/products/q=Kentang"
        },
        {
          id: 3,
          name: "Padi",
          imgUrl: Padi,
          link: "http://kiostani-api.herokuapp.com/products/q=Kentang"
        },
        {
          id: 4,
          name: "Jagung",
          imgUrl: Jagung,
          link: "http://kiostani-api.herokuapp.com/products/q=Kentang"
        }
      ]
    }
  }

  render(){
    return(
      <div className="container">
        <div className="row">
          <Categories items={this.state.categories} />
        </div>
      </div>
    );
  } 
}

export default Dashboard;