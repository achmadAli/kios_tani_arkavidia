import React, { Component } from 'react';

import FormInput from '../../components/form-input/form-input.component';
import Button from '../../components/button/button.component';

class Authentication extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      name: '',
      email: '',
      password: ''
    }

  }

  handleSubmit = (event) => {
    event.preventDefault();
    console.log(this.state);
  }

  handleChange = event => {
    const {name, value} = event.target;
    this.setState({[name] : value })
  }

  render() {
    return (
      <div className="auth">
        <h3 className="auth__title"> Daftar </h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <FormInput required type="text" name="name" placeholder="E.g John Doe" label="Masukkan Nama" handleChange={this.handleChange} />
            <FormInput required type="email" name="email" placeholder="other@example.com" label="Masukkan email" handleChange={this.handleChange} />
            <FormInput required type="password" name="password" placeholder="Masukkan Password" label="Masukkan Password" handleChange={this.handleChange} />
          </div>

          <div className="btn-group">
          <Button type="submit"> Daftar </Button>
          </div>
        </form>
      </div>
    )
  }
}
export default Authentication;
