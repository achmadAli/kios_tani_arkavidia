import React from 'react';
import { Switch, Route } from 'react-router-dom';
import AdminSidebar from '../../components/admin-sidebar/admin-sidebar.component';
import AddNewMember from '../../components/admin-addMember/admin-addMember.component';
import AddNewPlant from '../../components/admin-addPlant/admin-addPlant.component';
import AddNewProduct from '../../components/admin-addProduct/admin-addProduct.component';
import AddNewHarvestData from '../../components/admin-addHarvestData/admin-addHarvestData.component';
import AddNewGrowData from '../../components/admin-addGrowData/admin-addGrowData.component';

const admin = (props) => {
  
  return (
    <div className="container">
      <div className="row">
        <div className="col-3">
          <AdminSidebar />
        </div>
        <div className="col-9">
          <Switch>
            <Route exact path="/admin/addMember" render={()=> <AddNewMember /> } />
            <Route exact path="/admin/addPlant" render={()=> <AddNewPlant /> } />
            <Route exact path="/admin/addGrowData" render={()=> <AddNewGrowData /> } />
            <Route path="/admin/addHarvestData" render={()=> <AddNewHarvestData /> } />
            <Route path="/admin/addProduct" render={()=> <AddNewProduct /> } />
          </Switch>         

        </div>
      </div>
    </div>
  )
}

export default admin;