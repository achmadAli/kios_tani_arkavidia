import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCartItems } from '../../redux/cart/cart.selectors';
import { addItem, removeItem, deleteProduct } from '../../redux/cart/cart.actions';
import CheckoutItems from '../../components/checkout-items/checkout-items.component'
import Button from '../../components/button/button.component';
const checkout = ({carts}) => {
  return (
    <div className="container">
      <div className="product-cart product-cart--header">
        <div className="product-cart__info">
          <p> Nama Produk </p>
        </div>
        <div className="product-cart__price">
          <p> Harga Barang </p>
        </div>
        <div className="product-cart__control">
          Jumlah Barang
        </div>
        <div className="product-cart__price">
          <p> Total Harga Barang </p>
        </div>
      </div>
      <CheckoutItems items={carts} />
      <Button type="button" isRightBtn> checkout </Button>
    </div>
  )
}

const mapDispatchToProps = dispatch => ({
  addItem: item => dispatch(addItem(item)),
  removeItem: item => dispatch(removeItem(item)),
  deleteProduct: item => dispatch(deleteProduct(item))
})

const mapStateToProps = createStructuredSelector({
  carts: selectCartItems
})


export default connect(mapStateToProps, mapDispatchToProps)(checkout);
