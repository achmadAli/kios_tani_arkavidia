import React, { Component } from 'react';
import Button from '../../components/button/button.component'

import './payment.styles.scss'

class Payment extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       files: '',
       imagePreviewUrl: ''
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    // const payment = {payment: this.state.file}
    console.log('handle uploading-', this.state.file);
  }

  handleImageChange(event) {
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file)
  }

  render() {

    let { imagePreviewUrl } = this.state;
    let imagePreview = imagePreviewUrl ? (<img src={imagePreviewUrl} className="preview__image" alt="foto_bukti" />) : (<div className="preview">Please select an Image for Preview</div>) ;

    return (
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-12">
            <form onSubmit={(e)=>this.handleSubmit(e)}>
              <input 
                className="fileInput" 
                type="file" 
                onChange={(e)=>this.handleImageChange(e)} />
              <Button type="submit" onClick={(e)=>this.handleSubmit(e)}> Upload </Button>
            </form>
          </div>

          <div className="col-lg-6 col-md-6 col-12">
            <div className="imgPreview">
              {imagePreview}
            </div>
          </div>

        </div>
      </div>
    )
  }
}

export default Payment