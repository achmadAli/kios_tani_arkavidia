import React from 'react';
import Products from '../../components/products/products.component';
import Kentang from '../../assets/img/kentang.jpg';

const products = [
  {
    id: 1,
    name: "Kentang Kualitas super",
    price: 12000,
    imgUrl: Kentang,
    minimum: "10kg"
  },
  {
    id: 2,
    name: "Kacang Kualitas super",
    price: 15000,
    imgUrl: Kentang,
    minimum: "10kg"
  },
  {
    id: 3,
    name: "Beras Kualitas super",
    price: 22000,
    imgUrl: Kentang,
    minimum: "10kg"
  },
  {
    id: 4,
    name: "Wortel Kualitas super",
    price: 4000,
    imgUrl: Kentang,
    minimum: "10kg"
  },
  {
    id: 5,
    name: "Labu Kualitas super",
    price: 8000,
    imgUrl: Kentang,
    minimum: "10kg"
  },
  {
    id: 6,
    name: "sayur Kualitas super",
    price: 6000,
    imgUrl: Kentang,
    minimum: "10kg"
  },
]

const listCategory = () => {
  return (
    <div className="container">
      <div className="row">
        <Products items={products} />
      </div>
    </div>
  )
}

export default listCategory;
