import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addItem, removeItem } from '../../redux/cart/cart.actions'
import Control from '../../components/quantity-control/quantity-control.component';
import Button from '../../components/button/button.component';
import './detail-product.styles.scss';


class DetailProduct extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      ...props.location.state.item,
      quantity: 1
    }
  }
  
  addPreCart = (item) => this.setState({...this.state, quantity: ++item.quantity});
  removePreCart = (item) => item.quantity !== 1 ? this.setState({...this.state, quantity: --item.quantity}) : 1;
  
  render(){
    const {addToCart, history} = this.props;
    const {name, imgUrl, price} = this.state;
    const checkout = item => {
      addToCart(item);
      history.push('/checkout')
    }
    return (
      <div className="container">
        <div className="product">
          <div className="product__image-wrapper">
            <img src={imgUrl} alt="foto_product"/>
          </div>
          <div className="product__user-control">
            <h1 className="product__name"> {name}</h1>
            <p className="product__price"> Rp.<b> {price} </b> /Kg </p>
            <div className="product__minimum"> 
              <h3> Jumlah </h3>
              <p> Minimum Pemberlian adalah 10 Kg</p>
            </div>
            <Control 
              item={this.state} 
              add={this.addPreCart}
              remove={this.removePreCart} />
            <Button type="button" onClick={()=> checkout(this.state)}> Beli </Button>
            <Button type="button" outlined onClick={() => addToCart(this.state)}> Tambah Ke Keranjang </Button>
          </div>
        </div>
        <div className="store">
          <h1> Profil Kios</h1>
          <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci ipsum quae magni minus quisquam cumque molestias, aliquid soluta dolorem expedita? Reiciendis, ab, expedita consequatur omnis doloremque et voluptate quam corporis maiores vitae natus, doloribus tenetur unde! Dignissimos sapiente repellat quasi cumque quaerat blanditiis iure deserunt. Deserunt natus laborum repellat neque? </p>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  addToCart: item => dispatch(addItem(item)),
  removeItem: item=> dispatch(removeItem(item))
})

export default connect(null, mapDispatchToProps)(DetailProduct);