import userTypes from './user.types';

export const setUser = item =>({
  type: userTypes.SET_USER,
  payload: item
})

export const logout = () => ({
  type: userTypes.LOGOUT_USER,
})