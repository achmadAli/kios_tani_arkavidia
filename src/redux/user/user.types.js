const actionTypes = {
  SET_USER : 'SET_USER',
  LOGOUT_USER: 'LOGOUT_USER'
} 

export default actionTypes