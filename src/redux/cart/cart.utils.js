import CartTypes from './cart.types';

const incrementItem = (item) => ({...item, quantity: ++item.quantity})
const decrementItem = (item) => ({...item, quantity: --item.quantity})
const updateItem    = (type, item) => CartTypes.ADD_ITEM === type ? incrementItem(item) : decrementItem(item)

export const updateCart = (cartItems, action) => {  
  const {type, payload} = action
  const isItemExist = cartItems.find(item => item.id === payload.id)

  if(isItemExist){
    return cartItems.map(item => item.id === payload.id ? updateItem(type, item) : {...item})
  }

  return  [...cartItems, {...payload}];
}

export const deleteProduct = (cartItems, action) => {
  const {payload} = action;

  return cartItems.filter(item => item.id !== payload.id)
}