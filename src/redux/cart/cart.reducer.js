import CartTypes from './cart.types';
import { updateCart, deleteProduct } from './cart.utils'

const INITIAL_STATE = {
  isCartShowed: false,
  items: []
}

const cartReducers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CartTypes.CART_TOGGLE:
      return ({
        ...state,
        isCartShowed: !state.isCartShowed
      })
    case CartTypes.ADD_ITEM:
      return ({
        ...state,
        items: updateCart(state.items, action)
      })
    case CartTypes.REMOVE_ITEM:
      return ({
        ...state,
        items: updateCart(state.items, action)
      })
    case CartTypes.DELETE_PRODUCT: 
      return ({
        ...state,
        items: deleteProduct(state.items, action)
      })
    default:
      return state
  }
}

export default cartReducers;