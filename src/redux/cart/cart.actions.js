import CartTypes from './cart.types';

export const toggleCart = () => ({
  type: CartTypes.CART_TOGGLE
})

export const addItem = item => ({
  type: CartTypes.ADD_ITEM,
  payload: item
})

export const removeItem = item => ({
  type: CartTypes.REMOVE_ITEM,
  payload: item
})

export const deleteProduct = item => ({
  type: CartTypes.DELETE_PRODUCT,
  payload: item
})

export const deleteAll = () => ({
  type: "deleteAll",
})