import { createSelector } from 'reselect';

const selectCart        = state => state.cart;
const totalCartReducer  = (total, item) => total + item.quantity; 
const totalPriceReducer = (total, item) => total + (item.quantity * item.price);

export const selectCartWrapper = createSelector(
  [selectCart],
  cart => cart.isCartShowed
);

export const selectCartItems = createSelector(
  [selectCart],
  cart => cart.items
)

export const selectTotalItem = createSelector(
  [selectCart],
  cart => cart.items.reduce(totalCartReducer, 0)
)

export const selectTotalPrice = createSelector(
  [selectCart],
  cart => cart.items.reduce(totalPriceReducer, 0)
)

