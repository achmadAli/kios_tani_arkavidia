import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { selectCurrentUser } from './redux/user/user.selectors';
import { logout } from './redux/user/user.actions'

import Dashboard from './pages/dashboard/dashboard.component';
import Authentication from './pages/authentication/authentication.component'
import DetailProduct from './pages/detail-product/detail-product.component'
import Checkout from './pages/checkout/checkout.component';
import Payment from './pages/payment/payment.component';
import ListCategory from './pages/list-category/list-category.component';
import SignUp from './pages/signup/signup.component';
import Admin from './pages/admin/admin.component';
import Navbar from './components/navbar/navbar.component';
import './App.css';

function App({user, logout}) {
  return (
    <main>
      <BrowserRouter>
        <Navbar />
        <div className="app-wrapper">
          <Switch>
            <Route exact path="/" component={Dashboard} />
            <Route path="/auth" render={(props)=>{
              if(user){
                props.history.push('/')
              } else {
                return <Authentication />
              }
            }} />
            <Route path="/signup" component={SignUp} />
            <Route path="/products/:name" component={ListCategory} />
            <Route path="/product/:id" component={DetailProduct} />
            <Route path="/checkout" component={Checkout} />
            <Route path="/payment" component={Payment} />
            <Route path="/admin-login" component={Authentication} />
            <Route exact path="/admin/" component={Admin} />
            <Route path="/admin/addMember" component={Admin} />
            <Route path="/admin/addPlant" component={Admin} />
            <Route path="/admin/addGrowData" component={Admin} />
            <Route path="/admin/addHarvestData" component={Admin} />
            <Route path="/admin/addProduct" component={Admin} />
            {/* <Route path="/logout" render={} /> */}
          </Switch>
        </div>
      </BrowserRouter>
    </main>
  );
}

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout())
})

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
