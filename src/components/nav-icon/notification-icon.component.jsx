import React from 'react'

import { ReactComponent as IconBell } from '../../assets/img/bell.svg';
import './nav-icon.styles.scss';

const notification = () => {
  return (
    <div className="nav-icon">
      <IconBell className="nav-icon__item" />
      {/* <div className="nav-icon__item-counter"> 0 </div> */}
    </div>
  )
}


export default notification;