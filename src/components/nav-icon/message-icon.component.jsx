import React from 'react'

import { ReactComponent as IconMessage } from '../../assets/img/comment.svg';
import './nav-icon.styles.scss';

const message = () => {
  return (
    <div className="nav-icon">
      <IconMessage className="nav-icon__item" />
      {/* <div className="nav-icon__item-counter"> 0 </div> */}
    </div>
  )
}

export default message;