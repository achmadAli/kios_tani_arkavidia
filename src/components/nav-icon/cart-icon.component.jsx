import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectTotalItem } from '../../redux/cart/cart.selectors';
import { ReactComponent as IconCart } from '../../assets/img/shopping-cart.svg';
import './nav-icon.styles.scss';

const cartIcon = ({toggleCart, totalCart}) => {
  return (
    <div className="nav-icon" onClick={()=> toggleCart()}>
      <IconCart className="nav-icon__item" />
      {
        totalCart ? (<div className="nav-icon__item-counter"> {totalCart} </div>) : ''
      }
    </div>
  )
}

const mapStateToProps = createStructuredSelector({
  totalCart: selectTotalItem
})

export default connect(mapStateToProps)(cartIcon);