import React from 'react';
import './button.styles.scss';

const secondaryBtn = isSecondary => isSecondary ? 'btn--secondary' : '';
const outlineBtn = outlined => outlined ? 'btn--outline' : '';
const rightBtn = isRightBtn => isRightBtn ? 'btn--right' : '';

const button = ({children, isSecondary, outlined, isHalf, isRightBtn, ...otherProps}) => {
  return (
    <button className={`btn ${secondaryBtn(isSecondary)} ${outlineBtn(outlined)} ${rightBtn(isRightBtn)}`} {...otherProps}>
      {children}
    </button>
  )
}

export default button;