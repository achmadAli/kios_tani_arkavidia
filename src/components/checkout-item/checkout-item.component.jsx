import React from 'react';
import { connect } from 'react-redux';
import { addItem, removeItem, deleteProduct } from '../../redux/cart/cart.actions';
import { ReactComponent as Trash } from '../../assets/img/bin.svg'
import QuantityControl from '../quantity-control/quantity-control.component';

import './checkout-item.styles.scss'

const checkoutItem = ({addItem, removeItem, deleteProduct, id, name, quantity, price, imgUrl}) => {
  const item = {id, name, quantity, price};
  return (
    <div className="product-cart">
      <div className="product-cart__info">
        <img src={imgUrl} alt="product-cart_photo"/>
        <p> {name} </p>
      </div>
      <div className="product-cart__price">
        <p> Rp. {price} </p>
      </div>
      <div className="product-cart__control">
        <QuantityControl isCheckout item={item} add={addItem} remove={removeItem} />
      </div>
      <div className="product-cart__price">
        <p> Rp. {quantity * price} </p>
      </div>
      <Trash className="product-cart__delete" onClick={()=> deleteProduct({id, name, quantity, price})} />
    </div>
  )
}

const mapDispatchToProps = dispatch => ({
  addItem: item => dispatch(addItem(item)),
  removeItem: item => dispatch(removeItem(item)),
  deleteProduct: item => dispatch(deleteProduct(item))
})

export default connect(null, mapDispatchToProps)(checkoutItem);
