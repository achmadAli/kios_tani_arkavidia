import React from 'react';

import { ReactComponent as PlusIcon } from '../../assets/img/plus.svg';
import { ReactComponent as MinusIcon } from '../../assets/img/minus.svg';

import './quantity-control.styles.scss';

const quantityControl = ({item, add, remove, isCheckout}) => {
  console.log(item);
  
  return (
    <div className={`control ${isCheckout ? 'control--no-margin' : ''}`}>
      <MinusIcon className="control__item" onClick={()=> remove(item)} />
      <div className="control__quantity"> {item.quantity} </div>
      <PlusIcon className="control__item" onClick={()=> add(item)} />
    </div>
  )
}

export default quantityControl;
