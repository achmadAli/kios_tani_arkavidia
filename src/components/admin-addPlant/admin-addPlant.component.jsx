import React, { Component } from 'react';
import FormInput from '../form-input/form-input.component';
import Button from '../button/button.component';

class addNewPlant extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       name: '',
       yields: ''
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    console.log(this.state);
  }

  handleChange = event => {
    const {name, value} = event.target;
    this.setState({[name] : value })
  }
  
  render() {
    return (
      <div className="container">
          <h3> Form Rencana Penanaman </h3>
        <form onSubmit={this.handleSubmit}>
          <FormInput required type="text" name="name" placeholder="Kentang" label="Masukkan Nama Calon Anggota" handleChange={this.handleChange} />
          <FormInput required type="text" name="yields" placeholder="3000" label="Masukkan Luas Lahan" handleChange={this.handleChange} />

          <Button type="submit"> Submit </Button>
        </form>
      </div>
    )
  }
}

export default addNewPlant;