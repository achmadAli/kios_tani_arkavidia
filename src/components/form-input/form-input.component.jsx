import React from 'react';

import './form-input.styles.scss';

const formInput = ({label, handleChange, ...otherprops}) => {
  return (
    <div className="form">
      <label htmlFor="input" > {label} </label>
      <input className="form__input" onChange={handleChange} {...otherprops} />
    </div>
  )
}

export default formInput;