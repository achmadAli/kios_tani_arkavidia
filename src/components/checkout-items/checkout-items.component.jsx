import React from 'react';
import CheckoutItem from '../checkout-item/checkout-item.component';

const checkoutItems = ({items}) => {
  return (
    <React.Fragment>
      {
        items.map(item => <CheckoutItem key={item.id} {...item} />)
      }
    </React.Fragment>
  )
}

export default checkoutItems