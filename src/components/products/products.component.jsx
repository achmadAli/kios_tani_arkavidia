import React from 'react';
import Card from '../card/card.component';

const products = ({items}) => {
  return (
    <React.Fragment>
      {
        items.map(item => (
          <div key={item.id} className="col-lg-3 col-md-3 col-12 mb-4">
            <Card {...item} />
          </div>
        ))
      }
    </React.Fragment>
  )
}

export default products;