import React from 'react';
import { withRouter } from 'react-router-dom';
import './category-card.styles.scss'


const categoryCard = ({id, name, imgUrl, link, history}) => {
  const redirect = link => history.push(`products/3`);
  return (
    <div className="category" onClick={()=> redirect(link)}>
      <div className="category__image-wrapper">
        <img src={imgUrl} alt="foto_category"/>
      </div>
      <div className="category__overlay">
        <h1 className="text-white"> {name} </h1>
      </div>
    </div>
  )
}

export default withRouter(categoryCard);