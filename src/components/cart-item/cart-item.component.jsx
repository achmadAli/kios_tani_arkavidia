import React from 'react';
import './cart-item.styles.scss';

const cartItem = ({id, name, price, quantity, imgUrl}) => {
  return (
    <div className="cart-item">
      <img src={imgUrl} alt="foto_product"/>
      <div className="cart-item__info">
        <p> {name} </p>
        <p> {price} <span> X {quantity} </span> </p>
      </div>
    </div>
  )
}

export default cartItem;