import React from 'react';
import { withRouter } from 'react-router-dom';
import Button from '../button/button.component';

import './card.styles.scss'

const card = (props) => { 
  const {id, name, price, imgUrl, history} = props;
  const redirect = id => history.push({
    pathname: `/product/${id}`, 
    state: {
      item : {
        id: id, 
        name: name, 
        price: price, 
        imgUrl: imgUrl
      }
    }
  })
  return (
    <div className="card">
      <img src={imgUrl} alt="foto_product" className="card__image"/>
      <div className="card__detail">
        <h3 className="card__title"> {name} </h3>
        <p className="card__description"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis natus repellendus reiciendis optio, a itaque impedit numquam eos cum molestias!</p>
        <p className="card__price">Rp {price} </p>
        <small className="card__stock"> Stok tersedia : 240kg</small>
        <Button onClick={()=> redirect(id)}> Beli </Button>
      </div>
    </div>
  )
}

export default withRouter(card);