import React, { Component } from 'react';
import FormInput from '../form-input/form-input.component';
import Button from '../button/button.component';
import Select from 'react-select';

class addGrowData extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       plant: '',
       landArea: '',
       farmer: ''
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    console.log(this.state);
  }

  handleChange = event => {
    const {name, value} = event.target;
    this.setState({[name] : value })
  }

  handleSelect = event => {
    this.setState({farmer: event.value})
  }
  
  render() {

    const options = [
      {value: 'blues', label: 'Blues' },
      {value: 'rock', label: 'Rock' },
      {value: 'jazz', label: 'Jazz' },
      {value: 'orchestra', label: 'Orchestra' } 
    ];

    return (
      <div className="container">
          <h3> Form Penanaman </h3>
        <form onSubmit={this.handleSubmit}>
          <FormInput required type="text" name="plant" placeholder="John Doe" label="Masukkan Nama Calon Anggota" handleChange={this.handleChange} />
          <FormInput required type="text" name="landArea" placeholder="123456778899" label="Masukkan NIK Calon Anggota" handleChange={this.handleChange} />
          <FormInput required type="text" name="farmer" placeholder="3000" label="Masukkan Luas Lahan" handleChange={this.handleChange} />
          <Select 
            name="farmer"
            options={options}
            onChange={this.handleSelect}
          />
          <Button type="submit"> Submit </Button>
        </form>
      </div>
    )
  }
}

export default addGrowData;