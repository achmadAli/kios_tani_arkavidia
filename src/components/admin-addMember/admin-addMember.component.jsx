import React, { Component } from 'react';
import FormInput from '../form-input/form-input.component';
import Button from '../button/button.component';

class addMember extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       name: '',
       identityNumber: '',
       landArea: ''
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    console.log(this.state);
  }

  handleChange = event => {
    const {name, value} = event.target;
    this.setState({[name] : value })
  }
  
  render() {
    return (
      <div className="container">
          <h3> Form Pendaftaran Anggota Baru </h3>
        <form onSubmit={this.handleSubmit}>
          <FormInput required type="text" name="name" placeholder="John Doe" label="Masukkan Nama Calon Anggota" handleChange={this.handleChange} />
          <FormInput required type="text" name="identityNumber" placeholder="123456778899" label="Masukkan NIK Calon Anggota" handleChange={this.handleChange} />
          <FormInput required type="text" name="landArea" placeholder="3000" label="Masukkan Luas Lahan" handleChange={this.handleChange} />

          <Button type="submit"> Submit </Button>
        </form>
      </div>
    )
  }
}

export default addMember;