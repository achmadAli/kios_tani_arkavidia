import React from 'react';
import { Link } from 'react-router-dom';

const adminSidebar = () => {
  return (
    <ul className="list-group">
      <li className="list-group-item"> <Link to="/admin/addMember"> Tambah Anggota </Link> </li>
      <li className="list-group-item"> <Link to="/admin/addPlant"> Tambah penanaman </Link> </li>
      <li className="list-group-item"> <Link to="/admin/addGrowData"> Tambah Info Penanaman </Link> </li>
      <li className="list-group-item"> <Link to="/admin/addHarvestData"> Tambah Data Panen </Link> </li>
      <li className="list-group-item"> <Link to="/admin/addProduct"> Tambah Produk </Link> </li>
    </ul>
  )
}

export default adminSidebar;