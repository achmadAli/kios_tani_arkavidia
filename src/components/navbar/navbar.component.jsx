import React from 'react';
import { connect } from 'react-redux';
import { toggleCart } from '../../redux/cart/cart.actions';
import { createStructuredSelector } from 'reselect'
import { selectCartWrapper } from '../../redux/cart/cart.selectors';
import { selectCurrentUser } from '../../redux/user/user.selectors';
import { Link } from 'react-router-dom';
import CartIcon from '../nav-icon/cart-icon.component';
import CartDropdown from '../cart-dropdown/cart-dropdown.component';
import NotificationIcon from '../nav-icon/notification-icon.component';
import MessageIcon from '../nav-icon/message-icon.component'
import './navbar.styles.scss'

const Navbar = ({toggleCart, isCartShowed, user}) => {
    
  return (
    <nav className="navbar">
      <div className="navbar__wrapper container">
        <Link to="/">
          <h3 className="navbar__title"> KIOS TANI </h3>
        </Link>

        <div className="navbar__user-control">
          <CartIcon toggleCart={toggleCart} />
          <NotificationIcon />
          <MessageIcon />
        </div>
        <div className="navbar__user">
          <img src="https://robohash.org/0S8.png?set=set3&size=150x150" alt="profile_image"/>
          <p> {user ? user.user.name : "login" } </p>
        </div>
      </div>
      {isCartShowed ? <CartDropdown /> : '' }
    </nav>
  )
}

const mapDispatchToProps = dispatch => ({
  toggleCart: () => dispatch(toggleCart())
})

const mapStateToProps = createStructuredSelector({
  isCartShowed: selectCartWrapper,
  user: selectCurrentUser
})

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);