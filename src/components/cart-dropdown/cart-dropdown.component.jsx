import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import { createStructuredSelector } from 'reselect';
import { selectCartItems } from '../../redux/cart/cart.selectors';
import CartItem from '../cart-item/cart-item.component';
import Button from '../button/button.component';
import './cart-dropdown.styles.scss';

const cartDropdown = ({carts}) => {
  return (
    <div className="cart-dropdown">
      {
        carts.map(item => <CartItem key={item.id} {...item} /> )
      }
      <Link to="/checkout"> 
        <Button > checkout </Button>
      </Link>
    </div>
  )
}

const maptStateToProps = createStructuredSelector({
  carts: selectCartItems
})

export default connect(maptStateToProps)(cartDropdown);