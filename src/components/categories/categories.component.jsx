import React from 'react';
import CategoryCard from  '../category-card/category-card.component'

const categories = ({items}) => {
  return (
    <React.Fragment>
      {
        items.map(item => (
          <div key={item.id} className="col-lg-6 col-md-6 col-12 mb-4">
            <CategoryCard {...item} />
          </div>
        ))
      }
    </React.Fragment>
  )
}

export default categories;